/*
 * Copyright (c) 2013 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.enzo.notification;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;


/**
 * Created by
 * User: hansolo
 * Date: 01.07.13
 * Time: 07:10
 *
 * Modified by
 * User: plum
 * Date: 10.02.14
 *
 */
public class Notification {
    public static final Image INFO_ICON = new Image(Notifier.class.getResourceAsStream("info.png"));
    public static final Image WARNING_ICON = new Image(Notifier.class.getResourceAsStream("warning.png"));
    public static final Image SUCCESS_ICON = new Image(Notifier.class.getResourceAsStream("success.png"));
    public static final Image ERROR_ICON = new Image(Notifier.class.getResourceAsStream("error.png"));
    public final String TITLE;
    public final String MESSAGE;
    public final Image IMAGE;
    public final Duration LIFETIME;


    // ******************** Constructors **************************************
    public Notification(final String TITLE, final String MESSAGE) {
        this(TITLE, MESSAGE, null);
    }

    public Notification(final String MESSAGE, final Image IMAGE) {
        this("", MESSAGE, IMAGE);
    }

    public Notification(final String TITLE, final String MESSAGE, final Image IMAGE) {
        this(TITLE, MESSAGE, IMAGE, null);
    }

    public Notification(final String TITLE, final String MESSAGE, final Image IMAGE, final Duration LIFETIME) {
        this.TITLE = TITLE;
        this.MESSAGE = MESSAGE;
        this.IMAGE = IMAGE;
        this.LIFETIME = LIFETIME;
    }


    // ******************** Inner Classes *************************************
    public enum Notifier {
        INSTANCE;

        private static final double ICON_WIDTH = 24;
        private static final double ICON_HEIGHT = 24;
        private static double width = 300;
        private static double height = 0;
        private static double spacingY;
        private Duration popupLifetime;
        private Duration popupAnimationTime;
        private Stage stage;
        private Scene scene;
        private VBox box;
        private ObservableList<Pane> popups;

        // ******************** Constructor ***************************************
        private Notifier() {
            init();
            initGraphics();
        }


        // ******************** Initialization ************************************
        private void init() {
            popupLifetime = Duration.millis(3000);
            popupAnimationTime = Duration.millis(500);
            popups = FXCollections.observableArrayList();
            spacingY = 5;
        }

        private void initGraphics() {
            box = new VBox();
            box.setSpacing(spacingY);
            box.autosize();

            scene = new Scene(box);
            scene.setFill(null);
            scene.getStylesheets().add(getClass().getResource("notifier.css").toExternalForm());

            stage = new Stage();
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.setScene(scene);
            stage.setX(2);
            stage.setY(30);
            // Get more space for notifications than screen have
            stage.setHeight(Screen.getPrimary().getVisualBounds().getHeight()*3);
        }


        // ******************** Methods *******************************************

        /**
         * Sets the Notification's owner stage so that when the owner
         * stage is closed Notifications will be shut down as well.<br>
         * This is only needed if <code>setPopupLocation</code> is called
         * <u>without</u> a stage reference.
         *
         * @param OWNER
         */
        public static void setNotificationOwner(final Stage OWNER) {
            INSTANCE.stage.initOwner(OWNER);
        }

        /**
         * @param WIDTH The default is 300 px.
         */
        public static void setWidth(final double WIDTH) {
            Notifier.width = WIDTH;
        }

        /**
         * @param HEIGHT The default is auto sized.
         */
        public static void setHeight(final double HEIGHT) {
            Notifier.height = HEIGHT;
        }

        /**
         * @param SPACING_Y The spacing between multiple Notifications.
         *                  <br> The default is 5 px.
         */
        public static void setSpacingY(final double SPACING_Y) {
            Notifier.spacingY = SPACING_Y;
        }

        public void stop() {
            popups.clear();
            stage.close();
        }

        /**
         * Returns the Duration that the notification will stay on screen before it
         * will fade out.
         *
         * @return the Duration the popup notification will stay on screen
         */
        public Duration getPopupLifetime() {
            return popupLifetime;
        }

        /**
         * Defines the Duration that the popup notification will stay on screen before it
         * will fade out. The parameter is limited to values between 2 and 20 seconds.
         * Default value is 3 seconds.
         *
         * @param POPUP_LIFETIME
         */
        public void setPopupLifetime(final Duration POPUP_LIFETIME) {
            popupLifetime = Duration.millis(clamp(2000, 20000, POPUP_LIFETIME.toMillis()));
        }

        /**
         * Returns the Duration that the popup notification fade animation will take
         *
         * @return the Duration the popup notification fade animation will take
         */
        public Duration getPopupAnimationTime() {
            return popupAnimationTime;
        }

        /**
         * Defines the Duration that the popup notification fade animation will take.
         * The parameter is limited to values between 100 and 1000 milliseconds.
         * Default value is 200 milliseconds.
         *
         * @param POPUP_FADE_DURATION
         */
        public void setPopupAnimationTime(final Duration POPUP_FADE_DURATION) {
            popupAnimationTime = Duration.millis(clamp(100, 10000, POPUP_FADE_DURATION.toMillis()));
        }

        /**
         * Create and Show the given Notification on the screen
         *
         * @param NOTIFICATION
         */
        public void notify(final Notification NOTIFICATION) {
            Label title = new Label(NOTIFICATION.TITLE);
            title.getStyleClass().add("title");

            ImageView icon = new ImageView(NOTIFICATION.IMAGE);
            icon.setFitWidth(ICON_WIDTH);
            icon.setFitHeight(ICON_HEIGHT);

            Label message = new Label(NOTIFICATION.MESSAGE, icon);
            message.getStyleClass().add("message");

            VBox popupLayout = new VBox();
            popupLayout.getStyleClass().add("layout");
            popupLayout.setSpacing(10);
            popupLayout.setPadding(new Insets(10, 10, 10, 10));
            popupLayout.setMinHeight(height);
            popupLayout.setMinWidth(width);
            popupLayout.getChildren().addAll(title, message);

            StackPane popupContent = new StackPane();
            popupContent.getStyleClass().add("notification");
            popupContent.getChildren().addAll(popupLayout);

            final Pane POPUP = new Pane();
            POPUP.getChildren().addAll(popupContent);

            box.getChildren().add(0, POPUP);
            popups.add(POPUP);

            POPUP.setVisible(true);

            // Needed hack to get calculated Height of POPUP before animation
            stage.hide();
            stage.show();

            // popup fade in - first height animation from 0, next opacity from 0
            final double orgHeight = popupContent.getHeight();
            popupContent.setPrefHeight(0.0);
            popupContent.setOpacity(0.0);
            final Timeline tlFadeIn = new Timeline();
            final KeyValue kvFadeIn = new KeyValue(popupContent.prefHeightProperty(), orgHeight);
            final KeyFrame kfFadeIn = new KeyFrame(popupAnimationTime.divide(2), kvFadeIn);
            tlFadeIn.getKeyFrames().addAll(kfFadeIn);
            tlFadeIn.setOnFinished(fadeInAction -> {
                final Timeline tlOpacity = new Timeline();
                final KeyValue kvOpacity = new KeyValue(popupContent.opacityProperty(), 1.0);
                final KeyFrame kfOpacity = new KeyFrame(popupAnimationTime.divide(2), kvOpacity);

                tlOpacity.getKeyFrames().addAll(kfOpacity);
                tlOpacity.play();
            });
            tlFadeIn.play();

            // popup fade out - first opacity animation to 0, next height to 0
            final Timeline tlFadeOut = new Timeline();
            final KeyValue kvOpacity = new KeyValue(POPUP.opacityProperty(), 0.0);
            final KeyFrame kfOpacity = new KeyFrame(popupAnimationTime.divide(2), kvOpacity);

            tlFadeOut.getKeyFrames().addAll(kfOpacity);
            tlFadeOut.setDelay(NOTIFICATION.LIFETIME != null ? NOTIFICATION.LIFETIME : popupLifetime);
            tlFadeOut.setOnFinished(actionEvent -> {
                // Remove content of POPUP becouse of animation height performance problems
                popupContent.setPrefHeight(popupContent.getHeight());
                POPUP.getChildren().removeAll();
                final KeyValue kvHeight = new KeyValue(popupContent.prefHeightProperty(), 0.0);
                final KeyFrame kfHeight = new KeyFrame(popupAnimationTime.divide(2), kvHeight);
                Timeline tlHeight = new Timeline();
                tlHeight.getKeyFrames().addAll(kfHeight);
                tlHeight.setOnFinished(tlHeightAction -> {
                    box.getChildren().remove(POPUP);
                    popups.remove(POPUP);
                    // Close notifications stage if no more popups are visible
                    if (popups.size() == 0) {
                        this.stop();
                    }
                });
                tlHeight.play();
            });

            if (stage.isShowing()) {
                stage.toFront();
            } else {
                stage.show();
            }

            tlFadeOut.play();
        }

        /**
         * Show a Notification with the given parameters on the screen
         *
         * @param TITLE
         * @param MESSAGE
         * @param IMAGE
         */
        public void notify(final String TITLE, final String MESSAGE, final Image IMAGE) {
            notify(new Notification(TITLE, MESSAGE, IMAGE));
        }

        /**
         * Show a Notification with the given title and message and an Info icon
         *
         * @param TITLE
         * @param MESSAGE
         */
        public void notifyInfo(final String TITLE, final String MESSAGE) {
            notify(new Notification(TITLE, MESSAGE, Notification.INFO_ICON));
        }

        /**
         * Show a Notification with the given title and message and a Warning icon
         *
         * @param TITLE
         * @param MESSAGE
         */
        public void notifyWarning(final String TITLE, final String MESSAGE) {
            notify(new Notification(TITLE, MESSAGE, Notification.WARNING_ICON));
        }

        /**
         * Show a Notification with the given title and message and a Checkmark icon
         *
         * @param TITLE
         * @param MESSAGE
         */
        public void notifySuccess(final String TITLE, final String MESSAGE) {
            notify(new Notification(TITLE, MESSAGE, Notification.SUCCESS_ICON));
        }

        /**
         * Show a Notification with the given title and message and an Error icon
         *
         * @param TITLE
         * @param MESSAGE
         */
        public void notifyError(final String TITLE, final String MESSAGE) {
            notify(new Notification(TITLE, MESSAGE, Notification.ERROR_ICON));
        }

        /**
         * Makes sure that the given VALUE is within the range of MIN to MAX
         *
         * @param MIN
         * @param MAX
         * @param VALUE
         * @return
         */
        private double clamp(final double MIN, final double MAX, final double VALUE) {
            if (VALUE < MIN) return MIN;
            if (VALUE > MAX) return MAX;
            return VALUE;
        }
    }
}
