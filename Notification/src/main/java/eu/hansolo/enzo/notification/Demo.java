/*
 * Copyright (c) 2013 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.enzo.notification;

/**
 * Created by
 * User: hansolo
 * Date: 01.07.13
 * Time: 07:10
 */

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.Random;


public class Demo extends Application {
    private static final Random RND = new Random();
    private static final Image[] images = {Notification.INFO_ICON, Notification.WARNING_ICON, Notification.SUCCESS_ICON, Notification.ERROR_ICON};
    private static final Notification[] NOTIFICATIONS = {
            new Notification("Info", "New information\nLine one\nLine two", Notification.INFO_ICON),
            new Notification("Warning", "Attention, somethings wrong\nNew line", Notification.WARNING_ICON),
            new Notification("Success", "Great it works\nNew one\nLine two", Notification.SUCCESS_ICON),
            new Notification("Error", "ZOMG\nNew line", Notification.ERROR_ICON)
    };
    private Notification.Notifier notifier;
    private Button button;
    private Integer count = 0;


    // ******************** Initialization ************************************
    @Override
    public void init() {

        button = new Button("Notify");
        button.setOnAction(event -> {
            String lines = "";
            int genRandom = RND.nextInt(3) + 1;
            for (Integer i = 0; i < genRandom; i++) {
                lines += "Line " + i.toString() + "\n";
            }

            Duration duration = new Duration((RND.nextInt(10) + 1) * 1000);
            notifier.notify(new Notification("Notification: [" + count.toString() + "] (" + duration.toSeconds() + " sec)", lines, images[RND.nextInt(4)], duration));
            count++;
        });
    }

    // ******************** Application start *********************************
    @Override
    public void start(Stage stage) {
        notifier = Notification.Notifier.INSTANCE;

        StackPane pane = new StackPane();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.getChildren().addAll(button);

        Scene scene = new Scene(pane);
        stage.setOnCloseRequest(observable -> notifier.stop());
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() {
    }

    public static void main(String[] args) {
        launch(args);
    }
}
